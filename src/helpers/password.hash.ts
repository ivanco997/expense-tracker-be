import * as crypto from 'crypto';
import { throwError } from 'rxjs';

export function passwordHash(password: string) {
    try {
        return crypto.createHmac('sha256', password)
        .digest('hex');
    } catch (error) {
        throwError(error);
    }

}
