import { Entity, PrimaryGeneratedColumn, Column, OneToMany, BeforeInsert, BaseEntity } from 'typeorm';
import { Expense } from '../expense/entity/expense.entity';
import { passwordHash } from '../helpers/password.hash';

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    firstName: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    lastName: string;

    @Column({ type: 'varchar', length: 255, nullable: true })
    image: string;

    @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
    username: string;

    @Column({ select: false, type: 'varchar', length: 255, nullable: false })
    password: string;

    @Column({ type: 'decimal', nullable: true, default: 0 })
    amount: number;

    @OneToMany(type => Expense, expense => expense.user)
    expense: Expense[];

    @BeforeInsert()
    hashPassword() {
        this.password = passwordHash(this.password);
    }
}
