import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExpenseModule } from './expense/expense.module';
import { ConfigModule } from './config/config.module';
import { TypeormConfigService } from './config/typeormconfig/typeormconfig.service';
import { CategoryModule } from './category/category.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useClass: TypeormConfigService,
    }),
    ExpenseModule,
    CategoryModule,
    AuthModule,
    UserModule,
  ],
})
export class AppModule {}
