import {
  Injectable,
  UnprocessableEntityException,
  BadRequestException,
  HttpException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Expense } from '../entity/expense.entity';
import {
  Repository,
  DeepPartial,
  Between,
  FindOperator,
  LessThan,
  Brackets,
} from 'typeorm';
import { validate } from 'class-validator';
import { subDays, subMonths } from 'date-fns';
import { CategoryService } from '../../category/service/category.service';
import { IDashboard, ICategory } from '../../category/model/category-count.model';
import * as moment from 'moment';
import { UserService } from '@app/user/user.service';

@Injectable()
export class ExpenseService {
  private total: number = 0;

  constructor(
    @InjectRepository(Expense) private repository: Repository<Expense>,
    private categoryService: CategoryService,
    private userService: UserService,
  ) { }

  public async addExpense(
    entity: DeepPartial<Expense>,
    userId: string,
  ): Promise<Expense> {
    const user = await this.userService.findOneById(userId);
    const totalAmount = await this.getTotalAmount(userId);

    if (user.amount - totalAmount < entity.amount) {
      throw new HttpException('Monthly budget is full', HttpStatus.BAD_REQUEST);
    }

    if (entity.categoryName) {
      const foundCategory = await this.categoryService.findCategoryByName(
        entity.categoryName,
      );
      entity.categoryId = foundCategory.id;
    }
    const expense: Expense = this.repository.create(entity);
    expense.userId = userId;
    await this.validate(expense);
    return expense.save();
  }

  public async getExpenses(
    userId: string,
    skip: number,
    take: number,
    param?: string,
  ): Promise<{ result: Expense[]; total: number }> {
    if (!userId) {
      throw new BadRequestException('No userId provided');
    }

    const date: FindOperator<Date> | Date = this.getDateBetween(param);

    const result = await this.repository.find({
      where: {
        userId,
        date,
      },
      order: {
        date: 'DESC',
      },
      skip,
      take,
    });

    const total: number = await this.getTotalAmount(userId, param);
    return { result, total };
  }

  public async getDashboardData(userId: string, date: string): Promise<IDashboard> {
    const categories = await this.repository
      .createQueryBuilder('expense')
      .addSelect('category.name')
      .addSelect('COUNT(categoryId)', 'count')
      .addSelect('SUM(expense.amount)', 'totalCategoryAmount')
      .leftJoinAndSelect('expense.category', 'category')
      .where('expense.userId = :id', { id: userId })
      .andWhere('expense.date BETWEEN :startDate AND :endDate',
        { startDate: moment(this.getDateBetween(date, true) as Date).toISOString(true), endDate: moment(new Date()).toISOString(true) })
      .groupBy('categoryId')
      .getRawMany();

    const result: IDashboard = {
      categories: [],
      totalMonthlyAmount: 0,
    };

    for (const category of categories) {
      const categoryData: ICategory = {
        name: category.category_name,
        amount: category.totalCategoryAmount,
        count: category.count,
      };
      result.categories.push(categoryData);
    }

    result.totalMonthlyAmount = await this.getTotalAmount(userId);
    return result;
  }

  public async deleteExpense(expenseId: string): Promise<Expense> {
    const expense = await this.repository.findOne({
      where: {
        id: expenseId,
      },
    });

    if (!expense) {
      throw new NotFoundException('Expense not found');
    }
    await this.repository.delete({ id: expenseId });
    return expense;
  }

  async search(searchTerm: string, userId: string): Promise<{ result: Expense[]; total: number }> {
    const result = await this.repository.createQueryBuilder('expense')
      .where('expense.userId = :userId', { userId })
      .andWhere(
        new Brackets(qb => {
          qb.where('expense.title LIKE :title', { title: `${searchTerm}` })
            .orWhere('expense.amount LIKE :amount', { amount: `${searchTerm}` })
            .orWhere('expense.description LIKE :description', { description: `${searchTerm}` })
        })
      )
      .getMany();

    return { result, total: 0 }
  }

  private async getTotalAmount(userId: string, date: string = 'month'): Promise<number> {
    const total = await this.repository
      .createQueryBuilder('expense')
      .where('expense.userId = :id', { id: userId })
      .andWhere('expense.date BETWEEN :startDate AND :endDate',
        { startDate: moment(this.getDateBetween(date, true) as Date).toISOString(true), endDate: moment(new Date()).toISOString(true) })
      .addSelect('SUM(expense.amount)', 'amount')
      .getRawOne();
    return total.amount;
  }

  private getDateBetween(param: string, queryBuilder: boolean = false): FindOperator<Date> | Date {
    let date: FindOperator<Date>;
    let queryDate: Date;
    switch (param) {
      case 'week':
        date = Between(subDays(new Date(), 7), new Date());
        queryDate = subDays(new Date(), 7);
        break;
      case 'month':
        date = Between(subMonths(new Date(), 1), new Date());
        queryDate = subMonths(new Date(), 1);
        break;
      case 'today':
        const midnight = new Date();
        midnight.setHours(0, 0, 0, 0);
        date = Between(midnight, new Date());
        queryDate = midnight;
        break;
      default:
        date = LessThan(new Date());
        const oldDate = new Date();
        oldDate.setFullYear(1979);
        queryDate = oldDate;
    }

    return queryBuilder ? queryDate : date;
  }

  private async validate(entity: Expense) {
    const errors = await validate(entity);
    if (errors.length) {
      throw new UnprocessableEntityException(errors);
    }
  }
}
