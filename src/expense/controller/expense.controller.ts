import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  Get,
  Query,
  Delete,
} from '@nestjs/common';
import { ExpenseService } from '../service/expense.service';
import { Expense } from '../entity/expense.entity';
import { Response } from 'express';
import { IDashboard } from '@app/category/model/category-count.model';

@Controller('expense')
export class ExpenseController {
  constructor(private expenseService: ExpenseService) { }

  @Post()
  public async postExpense(
    @Body() expense: Expense,
    @Res() res: Response,
    @Query('userId') userId: string,
  ) {
    try {
      const result = await this.expenseService.addExpense(expense, userId);
      res.status(HttpStatus.CREATED).json(result);
    } catch (error) {
      res.status(HttpStatus.BAD_REQUEST).json(error);
    }
  }

  @Get()
  public async getExpenses(
    @Query('userId') userId: string,
    @Query('skip') skip: number,
    @Query('limit') limit: number,
    @Query('date') date: string,
  ): Promise<{ result: Expense[]; total: number }> {
    return await this.expenseService.getExpenses(userId, skip, limit, date);
  }

  @Get('categories')
  async getCategoriesCount(
    @Query('userId') userId: string,
    @Query('date') date: string,
  ): Promise<IDashboard> {
    return await this.expenseService.getDashboardData(userId, date);
  }

  @Get('search')
  async search(
    @Query('userId') userId: string,
    @Query('searchTerm') searchTerm: string,
  ): Promise<{ result: Expense[]; total: number }> {
    return await this.expenseService.search(searchTerm, userId);
  }

  @Delete()
  public async deleteExpense(
    @Query('expenseId') expenseId: string): Promise<Expense> {
    return await this.expenseService.deleteExpense(expenseId);
  }
}
