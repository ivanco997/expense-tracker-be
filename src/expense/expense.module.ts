import { Module } from '@nestjs/common';
import { ExpenseController } from './controller/expense.controller';
import { ExpenseService } from './service/expense.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Expense } from './entity/expense.entity';
import { CategoryModule } from '../category/category.module';
import { UserModule } from '@app/user/user.module';

@Module({
  imports: [ TypeOrmModule.forFeature([Expense]), CategoryModule, UserModule],
  controllers: [ExpenseController],
  providers: [ExpenseService],
})
export class ExpenseModule {}
