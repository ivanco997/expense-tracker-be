import { Controller, Get } from '@nestjs/common';
import { CategoryService } from '../service/category.service';
import { Category } from '../entity/category.entity';

@Controller('category')
export class CategoryController {
    constructor(private categoryService: CategoryService) {}

    @Get()
    public async getAll(): Promise<Category[]> {
        return await this.categoryService.getAll();
    }
}
