import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from '../entity/category.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CategoryService {
    constructor(@InjectRepository(Category) private repository: Repository<Category>) {}

    public async findCategoryByName(name: string): Promise<Category> {
        let categoryResult: Category;
        const foundCategory: Category = await this.repository.findOne({ name });
        if (!foundCategory) {
            const category = {
                name,
            };
            categoryResult = await this.repository.create(category).save();
            return categoryResult;
        }
        return foundCategory;
    }

    public async getAll(): Promise<Category[]> {
        return await this.repository.find();
    }
}
