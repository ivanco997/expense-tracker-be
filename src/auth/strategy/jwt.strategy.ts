import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { JwtPayload } from '../interface/jwt.payload.interface';
import { AuthService } from '../service/auth.service';
import { Request } from 'express';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly authService: AuthService) {
    super(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: 'pe4enakoko6ka',
        passReqToCallback: true,
      },
      async (req: Request, payload: JwtPayload, next: () => void) => await this.validate(req, payload, next),
    );
  }

  async validate(request: Request, payload: JwtPayload, done: (ex: UnauthorizedException, data: any) => void) {
    const user = await this.authService.validateUser(payload);
    if (!user || this.isTokenExpired(payload.exp)) {
      return done(new UnauthorizedException(), false);
    }
    request.user = user;
    done(null, user);
  }

  private isTokenExpired(expiration: number): boolean {
    const date = new Date(0);
    date.setUTCSeconds(expiration);
    return !(date.getTime() > new Date().getTime());
  }
}
