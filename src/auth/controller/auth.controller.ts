import { Controller, Get, UseGuards, Headers, Post, Body } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { AuthGuard } from '@nestjs/passport';
import { LoginUserDto } from '../../user/dto/login-user.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Get('verify')
    @UseGuards(AuthGuard('jwt'))
    public async verify(@Headers('authorization') token: string) {
        return this.authService.verifyToken(token);
    }

    @Post('login')
    public async login(@Body() credentials: LoginUserDto) {
        return await this.authService.createToken(credentials);
    }
}
